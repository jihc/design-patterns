package com.laurel.designpatterns.factory;

import com.laurel.designpatterns.factory.entity.Cappuccino;
import com.laurel.designpatterns.factory.entity.Coffee;
import com.laurel.designpatterns.factory.entity.Latte;

/**
 * 简单工厂
 * 实际不能算作一种设计模式，它引入了创建者的概念，将实例化的代码从应用代码中抽离，在创建者类的静态方法中只处理创建对象的细节，后续创建的实例如需改变，只需改造创建者类即可。
 * 但由于使用静态方法来获取对象，使其不能在运行期间通过不同方式去动态改变创建行为，因此存在一定局限性。
 *
 * @author Laurel
 * @date 2022-04-25
 */
public class SimpleFactory {
    /**
     * 通过类型获取Coffee实例对象
     *
     * @param type 咖啡类型
     * @return
     */
    public static Coffee createCoffee(String type) {
        switch (type) {
            case "cappuccino":
                return new Cappuccino();
            case "latte":
                return new Latte();
            default:
                throw new RuntimeException("type[" + type + "]类型不可识别，没有匹配到可实例化的对象！");
        }
    }

    public static void main(String[] args) {
        SimpleFactory.createCoffee("latte").mBuy();
        SimpleFactory.createCoffee("cappuccino").mBuy();
    }
}
