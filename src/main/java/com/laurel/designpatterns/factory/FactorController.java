package com.laurel.designpatterns.factory;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 工厂模式Controller
 *
 * @author Laurel
 * @date 2022-04-25
 */
@RestController("factorController")
@RequestMapping("/api/design-patterns/factor")
public class FactorController {

    @PostMapping("/simple")
    public void simple() {
        SimpleFactory.createCoffee("latte").cBuy();
        SimpleFactory.createCoffee("cappuccino").cBuy();
    }
}
