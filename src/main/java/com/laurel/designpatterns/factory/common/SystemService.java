package com.laurel.designpatterns.factory.common;

import org.springframework.stereotype.Service;

/**
 * @author Laurel
 * @date 2022-04-25
 */
@Service
public class SystemService {
    public void print(String param) {
        System.out.println(param);
    }
}
