package com.laurel.designpatterns.factory.entity;

import com.laurel.designpatterns.factory.common.SystemService;
import com.laurel.designpatterns.factory.util.ApplicationContextUtil;
import org.springframework.stereotype.Component;

/**
 * 咖啡（抽象概念）
 *
 * @author Laurel
 * @date 2022-04-25
 */
@Component
public abstract class Coffee {
    protected SystemService systemService;
    protected String str;

    public Coffee() {
        // main()调用时注释，接口请求时取消注释
//        this.systemService = (SystemService) ApplicationContextUtil.getBean("systemService");
        this.str = "您购买了一杯{coffee}！";
    }

    /**
     * controller调用
     */
    public abstract void cBuy();

    /**
     * main()调用
     */
    public abstract void mBuy();
}
