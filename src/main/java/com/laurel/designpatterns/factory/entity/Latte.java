package com.laurel.designpatterns.factory.entity;

import org.springframework.context.annotation.DependsOn;

/**
 * 拿铁
 *
 * @author Laurel
 * @date 2022-04-25
 */
@DependsOn("applicationContextUtil")
public class Latte extends Coffee {

    /**
     * controller调用
     */
    @Override
    public void cBuy() {
        systemService.print(str.replace("{coffee}", "卡布奇诺"));
    }

    /**
     * main()调用
     */
    @Override
    public void mBuy() {
        System.out.println(str.replace("{coffee}", "拿铁"));
    }
}
